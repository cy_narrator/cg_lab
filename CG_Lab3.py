from boreland_wrapper import *

def delay():
    time.sleep(0.1)

def symmetry(x,y,xc,yc,window_object):
    putpixel(xc+x,yc-y,window_object)
    delay()
    putpixel(xc + y,yc - x,window_object)
    delay()
    putpixel(xc+y,yc+x,window_object)
    delay()
    putpixel(xc + x,yc + y,window_object)
    delay()
    putpixel(xc - x,yc + y,window_object)
    delay()
    putpixel(xc - y,yc + x,window_object)
    delay()
    putpixel(yc - y,yc + x,window_object)
    delay()
    putpixel(yc - y,yc - x,window_object)
    delay()
    putpixel(xc - x,yc - y,window_object)
    delay()

def draw_circle(xc,yc,rad,window_object):
    x = 0
    y = rad
    p = 1 - rad
    for x in range(y):
        if p < 0:
            p += 2 * x + 3
        else:
            p += 2 * (x - y) + 5
            y -= 1
        symmetry(x,y,xc,yc,window_object)
        delay()

window = initgraph("Circle",800,600)
xc = int(input("Enter the center x coordinate: "))
yc = int(input("Enter the center y coordinate: "))
r = int(input("Enter the radius of the circle: "))
draw_circle(xc,yc,r,window)
pause()
closegraph(window)