from boreland_wrapper import *

x1 = int(input("Enter x-coordinate of first point:: "))
y1 = int(input("Enter the y-coordinate of first point: "))
x2 = int(input("Enter x-coordinate of second point: "))
y2 = int(input("Enter y-coordinate of second point "))
x = x1
y = y1
dx = x2 - x1
dy = y2 - y1
window = initgraph("BLE",800,600)
putpixel(x,y,window,True)
p = 2* dy - dx
while x <= x2:
    if p < 0:
        x = x + 1
        p = p + 2 * dy
    else:
        x = x + 1
        y = y + 1
        p = p + (2 * dy) - (2 * dx)
    putpixel(x,y,window,True)
    time.sleep(0.1)
pause()
closegraph(window,True)

