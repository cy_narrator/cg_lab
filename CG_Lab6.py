from boreland_wrapper import *
x1 = int(input("Enter starting point of line:x1 "))
y1 = int(input("Enter start of line:y1 "))
x2 = int(input("Enter end point of line:x1 "))
y2 = int(input("Enter end point of line:y1 "))
tx = int(input("Enter translation distance tx: "))
ty = int(input("Enter translation distance ty: "))
window = initgraph("Scaling", 800, 600)
line(x1,y1,x2,y2,window)
outtextxy(x2 + 2, y2 + 2, "Original Line", window)
x3 = x1 * tx
y3 = y1 * ty
x4 = x2 * tx
y4 = y2 * ty
line(x3,y3,x4,y4,window)
outtextxy(x4 + 2, y4 + 2, "Scaled", window)
pause()