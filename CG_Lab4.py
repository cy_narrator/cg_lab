from boreland_wrapper import *


def disp():
    time.sleep(0.05)
    putpixel(xc + x, yc + y, window)
    putpixel(xc - x, yc + y, window)
    putpixel(xc + x, yc - y, window)
    putpixel(xc - x, yc - y, window)

window = initgraph("Ellipse", 700, 600)
xc = int(input("Enter center x: "))
yc = int(input("Enter center y: "))
rx = int(input("Enter rx: "))
ry = int(input("Enter ry: "))
x = 0
y = ry
disp()
p1 = (ry * ry) - (rx * rx * ry) + (rx * rx)/4
while (2.0 * ry * ry * x) <= (2.0 * rx * rx * y):
    x += 1
    if p1 <= 0:
        p1 = p1 + (2.0 * ry * ry * x) + (ry * ry)
    else:
        y -= 1
        p1 = p1 + (2.0 * ry*ry*x) - (2.0 * rx * rx * y) + (ry*ry)
    disp()
    x = -x
    disp()
    x = -x
x = rx
y = 0
disp()
p2 = (rx * rx) + 2.0 * (ry*ry*rx) + (ry*ry)/4
while (2.0 * ry * ry *x) > (2.0*rx*rx*y):
    y += 1
    if p2 > 0:
        p2 = p2 + (rx*rx) - (2.0*rx*rx*y)
    else:
        x -= 1
        p2 = p2 + (2.0 * ry*ry*x) - (2.0*rx*rx*y) + (rx * rx)
    disp()
    y = -y
    disp()
    y = -y
pause()
closegraph(window)




