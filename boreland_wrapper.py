"""
SETUP:
    This script tries to satisfy it's own dependencies. However in some cases, it fails. So if you are getting ModuleNotFoundError, install graphics.py through pip.
    You can install graphics.py through pip as,
        1. Open your CMD or Terminal or Powershell or whatever
        2. Issue the command(not in python or python shell):
            pip install graphics.py
        3. Then simply import this file in your code


Don't worry whats in this file, it is just a collection of functions to make it similar to graphics.h library in Boreland's Turbo C compiler.
Though it is not possible to implement every features, most of what you need have been implemented.
There are a few things you need to know however,

    1. The initgraph function creates a window and returns a window object. That window object is what you pass on to other functions
    2. There is a weird also_print parameter in many functions which defaults to False, if you set it to True while calling, it will print to the console as well
    3. It accepts color strings ie. "AliceBlue" which are in Tk color specification: https://www.tcl.tk/man/tcl8.4/TkCmd/colors.html
    4. graphics.py in itself is an object oriented graphics library for Python by John Zelles for the book "Python Programming: An Introduction to Computer Science"
    5. You can find the documentation for graphics.py in here: https://mcsp.wartburg.edu/zelle/python/graphics/graphics/index.html but you do not need to know this just to use these wrapper functions

"""

try:
    from graphics import *

except ModuleNotFoundError:
    import os
    import time
    print("Seems like you did not install the required dependencies, let me do that for you\n")
    time.sleep(2)
    if os.name == 'nt':
        os.system('pip install -r requirements.txt')
    else:
        print("Need root permission to ensure proper installation: ")
        os.system('sudo pip install -r requirements.txt')
    print("\n")
    print("To remove the package, its 'pip uninstall graphics.py'")

else:
    import time
    import os

def initgraph(title, width=800, height=600, also_print=False):
    """Creates and returns that window as an object which you pass in other functions"""
    window = GraphWin(title,width,height)
    if also_print:
        print(f"Graphics window titled: {title} with size {width}x{height} created and called")
    return window


def pause():
    """Runs a pause command in your shell"""
    if os.name == 'nt':
        os.system('pause')
    elif os.name == 'posix':
        os.system('read -n1 -r -p "Press any key to continue..." key')
    else:
        print("Will pause for 5 seconds...")
        time.sleep(5)

    
def closegraph(window_object, also_print = False):
    """Closes the window provided the window object"""
    window_object.close()
    if also_print:
        print(f"Graphics window {window_object} closed")
    

def putpixel(x, y, window_object, also_print = False, color='black'): #Function to place a pixel on the screen given x and y coordinates
    """Puts a point in the specified location"""
    if also_print:
        print(f"Pixel was put in the position ({x},{y})")
    pt = Point(x,y)
    pt.setFill(color)
    pt.draw(window_object)


def outtextxy(x, y, text, window_object, font_color='black', font_size=10, also_print = False): #Function to place a text on the screen given x, y coordinates and text
    """Writes a text in a specified position in the screen"""
    if also_print:
        print(f"Text output: {text} on position ({x},{y}) of color={font_color}")
    message = Text(Point(x,y),text)
    message.setFill(font_color)
    message.setSize(font_size)
    message.draw(window_object)


def circle(center_x, center_y, radius, window_object, outline_color = 'black', fill_color = 'white', also_print = False):
    """Draws a circle given the center coordinate and the radius"""
    if also_print:
        print(f"Circle centered at ({center_x},{center_y}) drawn with radius {radius} of with border color={outline_color} and fill color={fill_color}")
    cir = Circle(Point(center_x,center_y),radius)
    cir.setOutline(outline_color)
    cir.setFill(fill_color)
    cir.draw(window_object)


def bar(top_left_x, top_left_y, bottom_right_x, bottom_right_y, window_object, outline_color='black', fill_color='white', also_print=False):
    """Draws a rectangle with opposite diagonals"""
    if also_print:
        print(f"Bar drawn from ({top_left_x},{top_left_y}) to ({bottom_right_x},{bottom_right_y}) with brder color={outline_color} and fill color={fill_color}")
    rect = Rectangle(Point(top_left_x,top_left_y), Point(bottom_right_x,bottom_right_y))
    rect.setOutline(outline_color)
    rect.setFill(fill_color)
    rect.draw(window_object)


def line(x1, y1, x2, y2, window_object, color='black', also_print = False, arrowhead='none'):
    """Draws a line with given two points"""
    if also_print:
        print(f"Line drawn from ({x1},{y1}) to ({x2},{y2}) with arrowhead attached at {arrowhead}")
    l = Line(Point(x1,y1),Point(x2,y2))
    l.setFill(color)
    l.setArrow(arrowhead)
    l.draw(window_object)
